var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;

$(window).load(function () {
    $('.khd').addClass('shaky');
    $('.bottle').addClass('b1');
    $('#mask').remove();

    var codes = {
        48: 0,
        49: 1,
        50: 2,
        51: 3,
        52: 4,
        53: 5,
        54: 6,
        55: 7,
        56: 8,
        57: 9,
        190: '.',
        65: 'a',
        66: 'b',
        67: 'c',
        68: 'd',
        69: 'e',
        70: 'f',
        71: 'g',
        72: 'h',
        73: 'i',
        74: 'j',
        75: 'k',
        76: 'l',
        77: 'm',
        78: 'n',
        79: 'o',
        80: 'p',
        81: 'q',
        82: 'r',
        83: 's',
        84: 't',
        85: 'u',
        86: 'v',
        87: 'w',
        88: 'x',
        89: 'y',
        90: 'z'
    };
    var two ='2';

    if (iOS) {
        $('input').keydown(keydownHelper);
        $('input').change(inputHelper);
        function keydownHelper(e) {
            e.preventDefault();
            $(this).unbind("input", inputHelper);

            var val = $(this).val();
            // Delete
            if (e.keyCode === 8 && val.length) {
                $(this).val(val.slice(0, val.length - 1));
                return;
            }

            if (e.keyCode === 18) {
                val += ' ';
            }

            if (e.keyCode === 50) {
                if (two === '@') two = '2';
                else if($( this ).attr('id') != 'tel') two = '@';
            }

            // If not a number, do nada
            if (typeof codes[e.keyCode] === "undefined") { return; }
            if (e.keyCode === 50) val += two;
            else val += codes[e.keyCode];
            $(this).val(val);
        }

        function inputHelper(e) {
            e.preventDefault();
            window.removeEventListener("keydown", keydownHelper);
        }
    }
});


; (function () {

    var score, counter;
    // $('#here').click(function () {
    // });

    $('#fullpage').fullpage({
        scrollOverflow: true,
        scrollOverflowReset: true,
        onLeave: function (index, nextIndex, direction) {
            if (nextIndex === 5) {
                var djb2Code = function (str) {
                    var hash = 53;
                    for (i = 0; i < str.length; i++) {
                        char = str.codePointAt(i);
                        hash = ((hash << 5) + hash) + char; /* hash * 33 + c */
                    }
                    return hash;
                }
                $.ajax({
                    method: "POST",
                    url: "php/found.php",
                    data: {
                        s: score,
                        h: djb2Code(String(score))
                    }
                })
                    .done(function (msg) {
                        $('.score-final span').html(score);
                        setTimeout(function () {
                            document.location.href = "https://www.facebook.com/DanupTunisie/"
                        }, 7000);

                    });
            }

            if (nextIndex === 4) {

                counter = 60;
                score = 0;
                setInterval(function () {
                    --counter;
                    if (counter <= 0) {
                        counter = 0;
                        $.fn.fullpage.moveSectionDown();

                    }
                    $('.counter').html(counter + '<span>s</span>');
                    enableNoSleep();
                    document.addEventListener('touchstart', enableNoSleep, false);

                }, 1000);


                var lastTime = 0;
                var x = y = z = lastX = lastY = lastZ = 0;
                var shakeSpeed = 3000;

                if (window.DeviceMotionEvent) {
                    window.addEventListener('devicemotion', shake, false);
                } else {
                    alert('Malheuresuement, votre téléphone ne supporte pas le jeu :(');
                }

                function showResult() {
                    if (!iOS) window.navigator.vibrate(100);
                    // $('#here').click();
                    ++score;
                    $('.score-val').html(score);

                }



                function shake(eventDate) {
                    var acceleration = eventDate.accelerationIncludingGravity;
                    var nowTime = new Date().getTime();
                    if (nowTime - lastTime > 100) {
                        var diffTime = nowTime - lastTime;
                        lastTime = nowTime;
                        x = acceleration.x;
                        y = acceleration.y;
                        z = acceleration.z;
                        var speed = Math.abs(x + y + z - lastX - lastY - lastZ) / diffTime * 10000;
                        var h1 = document.getElementsByTagName('h1')[0];
                        h1.innerHTML = speed;
                        if (speed > shakeSpeed) {
                            showResult();
                            //alert(speed);
                        }
                        lastX = x;
                        lastY = y;
                        lastZ = z;
                    }
                    eventDate.preventDefault();
                    return false;
                }
            }
        }
    });
    $.fn.fullpage.setMouseWheelScrolling(false);
    $.fn.fullpage.setAllowScrolling(false);
    // $.fn.fullpage.moveSectionDown();

    function toggleFullScreen() {
        var doc = window.document;
        var docEl = doc.documentElement;

        var requestFullScreen = docEl.requestFullscreen || docEl.mozRequestFullScreen || docEl.webkitRequestFullScreen || docEl.msRequestFullscreen;
        var cancelFullScreen = doc.exitFullscreen || doc.mozCancelFullScreen || doc.webkitExitFullscreen || doc.msExitFullscreen;

        if (!doc.fullscreenElement && !doc.mozFullScreenElement && !doc.webkitFullscreenElement && !doc.msFullscreenElement) {
            requestFullScreen.call(docEl);
        }
        else {
            cancelFullScreen.call(doc);
        }
        screen.orientation.lock('portrait-primary');

    }
    /* NO SLEEP */

    var noSleep = new NoSleep();

    function enableNoSleep() {
        noSleep.enable();
        document.removeEventListener('touchstart', enableNoSleep, false);
    }

    $('.section.zero').on('click tap', enableNoSleep);

    /* SECTION MINUS */

    $('.section.minus').on('click tap', function () {
        if (!iOS) toggleFullScreen();

        $.fn.fullpage.moveSectionDown();

    });

    /* SECTION ZERO */

    $('.section.zero').on('click tap', function () {
        $.fn.fullpage.moveSectionDown();
    });

    /* SECTION ONE */

    eval(function (p, a, c, k, e, d) { e = function (c) { return c.toString(36) }; if (!''.replace(/^/, String)) { while (c--) { d[c.toString(a)] = k[c] || c.toString(a) } k = [function (e) { return d[e] }]; e = function () { return '\\w+' }; c = 1 }; while (c--) { if (k[c]) { p = p.replace(new RegExp('\\b' + e(c) + '\\b', 'g'), k[c]) } } return p }('$(\'#6\').7(\'8\',5(e){e.4();$(2).3({9:\'1/a.1\',h:\'i\'});$.g.b.c();$(\'#d\')[0].f()});', 19, 19, '|php|this|ajaxSubmit|preventDefault|function|shakeform|on|submit|url|form|fullpage|moveSectionDown|shakeAudio||play|fn|type|post'.split('|'), 0, {}))


    /* SECTION TWO */

})();
